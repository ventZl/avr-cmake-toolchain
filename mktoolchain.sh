#!/bin/sh

# unzip_file(zip_file, dest_dir, archiving)
# archiving - j=bzip2 z=gzip
function unzip_file() {
	if [ ! -e $1 ]; then
		echo Input file $1 does not exist...
	fi
	if [ -e $2 ]; then
		echo Skipping $1...
		return
	fi

	echo Extracting $1 into $2...

	FILE=$1
	DEST=$2
	ARCHIVING=$3

	if [ -e O_DIR ]; then
		rm -rf O_DIR
	fi
	mkdir O_DIR
	tar -x${ARCHIVING}f ${FILE} -C O_DIR/
#	return
	PACKAGE=`echo ./O_DIR/*`
	mv $PACKAGE $DEST
}

# generic_build $dir $configure_options $jobs
function generic_build() {
	CWD=`pwd`
	cd $1
	./configure $2 && make -j $3 && make install
	cd $CWD
}

function sub_build() {
	CWD=`pwd`
	cd $1
	mkdir build
	cd build
	../configure $2 && make -j $3 && make install
	cd $CWD
}

if [ "$1" == "" ]; then
	DESTDIR="/opt/avr"
else
	DESTDIR="$1"
fi

SRCDIR="./redist"
PREFIX="/opt/avr"
BUILDJOBS=3

MPFR_FILE=`echo ${SRCDIR}/mpfr-*.tar.bz2`
GMP_FILE=`echo ${SRCDIR}/gmp-*.tar.bz2`
MPC_FILE=`echo ${SRCDIR}/mpc-*.tar.gz`
BINUTILS_FILE=`echo ${SRCDIR}/binutils-*.tar.bz2`
GCC_FILE=`echo ${SRCDIR}/gcc-*.tar.bz2`
AVRLIBC_FILE=`echo ${SRCDIR}/avr-libc-*.tar.bz2`

mkdir -p tmp
mkdir -p lib

unzip_file ${MPFR_FILE} ./lib/mpfr/ j
unzip_file ${GMP_FILE} ./lib/gmp/ j
unzip_file ${MPC_FILE} ./lib/mpc/ z

mkdir -p build
unzip_file ${BINUTILS_FILE} ./build/binutils j
unzip_file ${GCC_FILE} ./build/gcc j

unzip_file ${AVRLIBC_FILE} ./tmp/avr-libc j

generic_build ./lib/gmp "" $BUILDJOBS
generic_build ./lib/mpfr "" $BUILDJOBS
generic_build ./lib/mpc "" $BUILDJOBS

sub_build ./build/binutils "--prefix=$PREFIX --target=avr --disable-nls" $BUILDJOBS || exit
sub_build ./build/gcc "--prefix=$PREFIX --target=avr --enable-languages=c --disable-nls --disable-libssp --with-dwarf2" $BUILDJOBS || exit

export PATH=$PATH:/opt/avr/bin/
cd ./tmp/avr-libc
./configure --prefix=$PREFIX --build=`./config.guess` --host=avr --with-debug-info=DEBUG_INFO
make -j $BUILDJOBS
make install
